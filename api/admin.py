from django.contrib import admin
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, JsonResponse
from django.views.generic import View
import  json
import requests
class auth_admin(View):
    def get(self, request):
        array_data = [
            {"name": "张三", "age": "10"},
            {"name":"李四", "age": "12"},
        ]
        return JsonResponse(array_data, safe=False)
        return JsonResponse({
            "code": 1, "msg": "测试","self":''
        })

    def login(request):
        #return HttpResponse('你好,你进入了登录的api')
        # 接受参数
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                if user.is_superuser:
                    login(request,user)
                    requests.session['user_type']='admin'
                    return JsonResponse({
                        "code": 0, "msg": "登录成功"
                    })
                else:
                    return JsonResponse({
                        "code": 1, "msg": "请使用管理员账号登录"
                    })
            else:
                return JsonResponse({
                    "code": 1, "msg": "你已经被禁用"
                })
        else:
            return JsonResponse({
                "code": 1, "msg": "用户名和密码错误"
            })
    def loginout(self):
        return HttpResponse('你好,你进入了登出的api')

