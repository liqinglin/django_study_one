from django.db import models

# 用户表
class User(models.Model):
    user_id = models.AutoField(primary_key=True)
    openid = models.CharField(max_length=100)
    parent_id = models.IntegerField()
    gender = models.SmallIntegerField()
    avatarUrl = models.CharField(max_length=250)
    phone = models.CharField(max_length=11)
    nickName = models.CharField(max_length=30)
    is_disable = models.SmallIntegerField()
    last_login_time = models.DateTimeField(auto_now_add=True)
    last_exchange_time = models.DateTimeField(auto_now_add=True)
    sum_integral = models.SmallIntegerField(default=0)
    def __str__(self):
        return self.name

# 用户地址表
class UserAddress(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(to="User", to_field="user_id",on_delete=models.DO_NOTHING)
    consignee = models.CharField(max_length=20)
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=12)
    district_id = models.SmallIntegerField()
    city_id = models.SmallIntegerField()
    province_id = models.SmallIntegerField()
    is_default = models.SmallIntegerField()
