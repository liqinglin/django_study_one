# coding:utf-8
from .user import user
from .admin import auth_admin
from django.urls import path

urlpatterns = [


    #后台操作的路由
    #path('admin/login', auth_admin.login),
    path('admin', auth_admin.as_view()),
    path('admin/login', auth_admin.login),
    path('admin/loginout', auth_admin.loginout),


    #api操作的路由
    path('login', user.login),
    path('loginout', user.loginout),
]
