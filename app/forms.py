#coding:utf-8
from django import  forms
from django.forms import fields

class UserRegister(forms.Form):
    user_name=fields.CharField(max_length=18,required=True)
    password=fields.CharField(widget=forms.PasswordInput)