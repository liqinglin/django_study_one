from django.db import models

# Create your models here.
from django.db import models


# 用户表
# class User(models.Model):
#     user_id = models.AutoField(primary_key=True)
#     openid = models.CharField(max_length=100)
#     parent_id = models.IntegerField()
#     gender = models.SmallIntegerField()
#     avatarUrl = models.CharField(max_length=250)
#     phone = models.CharField(max_length=11)
#     nickName = models.CharField(max_length=30)
#     is_disable = models.SmallIntegerField()
#     last_login_time = models.DateTimeField(auto_now_add=True)
#     last_exchange_time = models.DateTimeField(auto_now_add=True)
#     sum_integral = models.SmallIntegerField(default=0)
#     def __str__(self):
#         return self.name
# 
# # 用户地址表
# class UserAddress(models.Model):
#     id = models.AutoField(primary_key=True)
#     user_id = models.ForeignKey(to="User", to_field="user_id",on_delete=models.DO_NOTHING)
#     consignee = models.CharField(max_length=20)
#     address = models.CharField(max_length=100)
#     phone = models.CharField(max_length=12)
#     district_id = models.SmallIntegerField()
#     city_id = models.SmallIntegerField()
#     province_id = models.SmallIntegerField()
#     is_default = models.SmallIntegerField()
# 
# 
# class Register(models.Model):
#     id = models.AutoField(primary_key=True)
#     user_name = models.CharField(max_length=20)
#     password = models.CharField(max_length=100)
#     create_time=models.IntegerField()


class User(models.Model):
    #主键id
    user_id = models.AutoField(primary_key=True)
    # 用户名
    name = models.CharField(max_length=50,default='')
    # 用户余额
    money = models.DecimalField(max_digits=20,decimal_places=2,default=0.00)
    # 用户和用户的学校关联
    #school = models.OneToOneField(School, on_delete=models.PROTECT, null=True)


class School(models.Model):
    # 学校名称
    name = models.CharField(max_length=50,default='')
    # 关联的用户
    user = models.OneToOneField(User, on_delete=models.PROTECT)


class UserAddress(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(to="User", to_field="user_id",on_delete=models.DO_NOTHING)
    consignee = models.CharField(max_length=20)
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=12)
    district_id = models.SmallIntegerField()
    city_id = models.SmallIntegerField()
    province_id = models.SmallIntegerField()
    is_default = models.SmallIntegerField()


class Goods(models.Model):
    # 商品名称
    goods_name = models.CharField(max_length=50)

class Order(models.Model):
    # 订单号
    order_no = models.CharField(max_length=50)
    # 支付状态
    pay_status = models.SmallIntegerField()
    # 用户 一对多  关联用户表,用户表用户删除的时候订单也删除
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    # 订单关联商品,通过中间表进行关联
    goods = models.ManyToManyField(Goods, through='OrderGoods')





class OrderGoods(models.Model):
    # 购买的商品数量
    number = models.IntegerField()
    # 一对多 关联订单
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    # 一对多 关联商品
    goods = models.ForeignKey(Goods, on_delete=models.PROTECT)