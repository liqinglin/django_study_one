# coding:utf-8
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from django.http import JsonResponse
from .models import User
import requests
import time
import json
import random
from .forms import UserRegister


class Lession(View):
    def get(self, request, name, age):
        return HttpResponse('my name is {0},age is {1}'.format(name, age))


# 音乐
class Music(View):
    DOBAN_REQUEST_URL = 'https://api.douban.com/v2/music/search?q={0}'

    def get(self, request):
        music_name = request.GET.get('music_name')
        if music_name:
            # 请求豆瓣
            data = requests.get(self.DOBAN_REQUEST_URL.format(music_name))
            # 转化json格式
            data = data.json()
            return JsonResponse({
                'code': 0,
                'message': '成功',
                'data': data
            })
        else:
            return JsonResponse({
                'code': 1,
                'message': '丢失参数',
                'data': []
            })


# 电影
class Movie(View):
    DOBAN_REQUEST_URL = 'https://api.douban.com/v2/movie/search?q={0}'

    def get(self, request):
        movie_name = request.GET.get('movie_name', '')
        if not movie_name:
            return JsonResponse({'code': 1, 'message': '电影名称不能为空', "data": []})
        url = self.DOBAN_REQUEST_URL.format(movie_name)
        try:
            request_result = requests.get(url)
        except Exception as e:
            return JsonResponse({'code': 1, 'message': '请求异常', "data": []})
        return JsonResponse({'code': 0, 'message': '成功', "data": request_result.json()})


# 用户出入
class Users(View):
    def get(self, request):
        name = '测试用户{0}'.format(random.randint(10000, 99999))
        User.objects.create(openid='openid', parent_id=0, gender=0, avatarUrl='http://www.baidu.com', phone=17326062026,
                            nickName=name, is_disable=0)
        return JsonResponse({
            'code': 0,
            'message': 'get成功{0}'.format(time.time()),
            'data': []
        })

    def post(self, request):
        user_id = request.GET.get('user_id')
        User.objects.filter(user_id=user_id).update(is_disable=1, openid='test')
        return JsonResponse({
            'code': 0,
            'message': 'post方法',
            'data': []
        })

    def put(self, request):
        return JsonResponse({
            'code': 0,
            'message': 'put方法',
            'data': []
        })

    def register(request):
        data = {
            'name': request.GET.get('name', '无名')
        }
        return JsonResponse({
            'code': 0,
            'message': '用户注册ok',
            'data': data
        })


class Register(View):
    TEMPLACE = 'register.html'

    def get(self, request):
        form = UserRegister()
        return render(request, self.TEMPLACE, {'form': form})

    def post(self, request):
        user_name = request.POST.get('user_name', '未获取到')
        password = request.POST.get('password', '')
        return HttpResponse('user_name {0}{1}'.format(user_name, password))
        pass


class Test():
    def get(self):
        list_data = {'name': '张三', 'age': 20}
        # 数据序列化
        #json_data=json.encoder(list_data)
        json_data=json.dumps(list_data)
        print(json_data)
        print(type(json_data))
        # 数据反序列化
        list_data = json.loads(json_data)
        print(list_data)
        print( type(list_data))
        return HttpResponse('user_name {0}{1}'.format('name', '12'))

# Create your views here.
# def index(request):
#     name= request.GET.get('name','无名')
#     age = request.GET.get('age',0)
#     return HttpResponse('my name is {0},age is {1}'.format(name,age))
# 列表
# def index(request, name, age):
#     context = {}
#     context['hello'] = 'Hello World!'
#     context['name'] = name
#     context['age'] = age
#     return render(request, 'app/index.html', context)
#     # name= request.GET.get('name','无名')
#     # age = request.GET.get('age',0)
#     return HttpResponse('my name is {0},age is {1}'.format(name, age))
#
# #增加
# def store(request):
#     # name= request.GET.get('name','无名')
#     # age = request.GET.get('age',0)
#     return HttpResponse('store')
#
# #更新
# def update(request, id):
#     # name= request.GET.get('name','无名')
#     # age = request.GET.get('age',0)
#     return HttpResponse('update {0} '.format(id))
#
# #删除
# def destroy(request, id):
#     # name= request.GET.get('name','无名')
#     # age = request.GET.get('age',0)
#     return HttpResponse('delete {0}'.format(id))
