# coding:utf-8
# from django.conf.urls import url
from django.urls import path
# from .views import index,update,destroy
# from .views import Lession
from .views import Music, Movie, Users, Register,Test
# from .TestView import TestView

# from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

urlpatterns = [
    # path('<str:name>/<int:age>',Lession.as_view()),
    path('music', Music.as_view()),
    path('movie', Movie.as_view()),
    path('user', Users.as_view()),
    path('register', Register.as_view()),
    path('user_register', Users.register),
    path('test', Test.get),
    # path('api/test', TestView.as_view()),
    # path('api/obtain', TokenObtainPairView.as_view()),
    # path('api/refresh', TokenRefreshView.as_view()),
    # url(r'^api/auth/token/refresh/$', TokenRefreshView.as_view()),  # 需要添加的内容
]
